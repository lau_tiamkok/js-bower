# Installation (Linux/ Xubuntu)

    Open a terminal and type the following command:

    `sudo npm install -g bower`

    Note that you must install it **globally**.

# Install packages

    Navigate to your project directory from the terminal and type:

    `bower install <package>`

    For instance,

    `bower install jquery`

# Create packages

    Navigate to your project directory from the terminal and type:

    `bower init`

    That generates a bower.json, in which you can define your dependencies:

    ```
    {
      "name": "RequireJS Starter",
      "version": "1.0.0",
      "dependencies": {
        "requirejs": "latest",
        "jquery": "latest"
      }
    }
    ```

# Change bower's default components folder

    Create a Bower configuration file .bowerrc in the project root (as opposed to your home directory) with the content:

    ```
    {
      "directory" : "public/components"
    }
    ```

    Run bower install again.

# References

1. http://blog.teamtreehouse.com/getting-started-bower
2. http://www.radcortez.com/javascript-package-management-npm-bower-grunt/
3. http://stackoverflow.com/questions/14079833/how-to-change-bowers-default-components-folder
